<?php

namespace KG\ScrollerBundle\Scroller;

use KG\ScrollerBundle\Scroller\ScrollerInterface;

/**
 * A base class for all the scrollers to extend from.
 *
 * @author Kristen Gilden <gilden@planet.ee>
 */
abstract class Scroller implements ScrollerInterface
{
    /**
     * @var array
     */
    protected $parameters;

    /**
     * @var integer
     */
    protected $type;

    /**
     * @var array
     */
    private $data;

    /**
     * @var boolean
     */
    private $loaded;

    /**
     * (@inheritDoc)
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * (@inheritDoc)
     */
    public function setType($type)
    {
        $sides = array(self::SIDE_TOP, self::SIDE_LEFT, self::SIDE_RIGHT, self::SIDE_BOT);

        // Removes the corner bits if the side is not set.
        //
        // Example: we have set TOP | LEFT = 00010011 | 00100101 = 00110111
        // We need to unset both bottom left and top right corners that are
        // currently set (bits 00000100 and 00000010).
        //
        // If a side is not set, the flipped bits of that side value masks
        // out the incorrect corners.
        //
        //   00110111
        // & 10110101  - flipped SIDE_RIGHT
        // & 01110011  - flipped SIDE_BOT
        // -----------
        //   00110001  - translates to TOP, LEFT and TOP_LEFT components

        $type_cut = $type;

        foreach ($sides as $i => $side) {
            if (($type & $side) !== $side) {
                $type_cut = $type_cut & ~$side;
            }
        }

        $this->type = $type_cut;

        return $this;
    }

    /**
     * (@inheritDoc)
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * (@inheritDoc)
     */
    public function hasAt($position)
    {
        return ($this->type & $position) === $position;
    }

    /**
     * (@inheritDoc)
     */
    public function getAt($position)
    {
        if ($this->loaded !== true) {
            $this->init();
            $this->load();
            $this->loaded = true;
        }

        if (isset($this->data[$position])) {
            return $this->data[$position];
        } else {
            return null;
        }
    }

    /**
     * Sets the data at the specified position.
     *
     * @param integer $position
     */
    protected function setAt($position, $data)
    {
        $this->data[$position] = $data;
    }

    /**
     * Initializes the class.
     */
    private function init()
    {
        $this->data = array(
            self::POS_TOP_LEFT  => null,
            self::POS_TOP_MID   => null,
            self::POS_TOP_RIGHT => null,
            self::POS_MID_LEFT  => null,
            self::POS_MID_MID   => null,
            self::POS_MID_RIGHT => null,
            self::POS_BOT_LEFT  => null,
            self::POS_BOT_MID   => null,
            self::POS_BOT_RIGHT => null,
        );
    }

    /**
     * Loads and populates the correct positions with data.
     */
    abstract protected function load();
}