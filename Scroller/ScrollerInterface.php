<?php

namespace KG\ScrollerBundle\Scroller;

/**
 * A scroller is an interactive component that is used for displaying
 * large amounts of data in a confined space. This class acts as the
 * data provider for such a componenet.
 *
 * The scroller has 9 areas to display data in:
 *
 * +---+---+---+
 * |   | t |   |
 * +---+---+---+
 * | l | m | r |
 * +---+---+---+
 * |   | b |   |
 * +---+---+---+
 *
 * Indexed squares are interactive: the user can use his mouse to drag
 * the data and it scrolls accordingly.
 *
 * @author Kristen Gilden <gilden@planet.ee>
 */
interface ScrollerInterface
{
    /*
     *  The values are chosen specifically to work together with the side
     *  types. Please do not change these values unless you have a clear
     *  idea how this works!
     */
    const POS_TOP_LEFT  = 1;
    const POS_TOP_MID   = 16;
    const POS_TOP_RIGHT = 2;
    const POS_MID_LEFT  = 32;
    const POS_MID_MID   = 0;
    const POS_MID_RIGHT = 64;
    const POS_BOT_LEFT  = 4;
    const POS_BOT_MID   = 128;
    const POS_BOT_RIGHT = 8;

    const SIDE_TOP   = 19;  // 16  + POS_TOP_LEFT + POS_TOP_RIGHT
    const SIDE_LEFT  = 37;  // 32  + POS_TOP_LEFT + POS_BOT_LEFT
    const SIDE_RIGHT = 74;  // 64  + POS_TOP_RIGHT + POS_BOT_RIGHT
    const SIDE_BOT   = 140; // 128 + POS_BOT_LEFT + POS_BOT_RIGHT

    /**
     * Set any additional parameters that the scroller might need for its
     * information.
     *
     * @param array $parameters
     */
    function setParameters(array $parameters);

    /**
     * Sets the scroll type. Only the following types are allowed: SIDE_TOP,
     * SIDE_LEFT, SIDE_RIGHT & SIDE_BOT.
     *
     * You can use a combination of types, eg self::SIDE_LEFT | self::SIDE_TOP
     * would provide the following template:
     *
     * +---+---+
     * |   | t |
     * +---+---+
     * | l | m |
     * +---+---+
     *
     * @param integer $type    Use the constants provided by the interface
     */
    function setType($type);

    /**
     * Gets the scroll type.
     */
    function getType();

    /**
     * Returns whether there is any data at the specified position. Use
     * the 9 POS_ constants specified above. You cannot use combinations.
     *
     * @param integer $position
     *
     * @return boolean
     */
    function hasAt($position);

    /**
     * Gets the data at the specified position.
     *
     * @param integer $position
     *
     * @return string
     */
    function getAt($position);
}