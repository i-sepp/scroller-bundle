<?php

namespace KG\ScrollerBundle\Tests\Provider;

use KG\ScrollerBundle\Provider\ContainerAwareProvider;

/**
 * @author Kristen Gilden <gilden@planet.ee>
 */
class ContainerAwareProviderTest extends \PHPUnit_Framework_TestCase
{

    public function testHasScroller()
    {
        $provider = new ContainerAwareProvider($this->getMockContainer(), array('foo' => 'baz'));

        $this->assertTrue($provider->has('foo'));
    }

    public function testGetInvalidThrowsException()
    {
        $this->setExpectedException('InvalidArgumentException');

        $provider = new ContainerAwareProvider($this->getMockContainer());

        $provider->get('foo');
    }

    public function testGetValidReturnsServiceFromContainer()
    {
        $scroller = $this->getMockScroller();

        $container = $this->getMockContainer();
        $container
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo('bar'))
            ->will($this->returnValue($scroller))
        ;

        $provider = new ContainerAwareProvider($container, array('foo' => 'bar'));

        $this->assertSame($scroller, $provider->get('foo'));
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockContainer()
    {
        return $this->getMock('Symfony\\Component\\DependencyInjection\\Container');
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockScroller()
    {
        return $this->getMockForAbstractClass('KG\\ScrollerBundle\\Scroller\\Scroller');
    }
}