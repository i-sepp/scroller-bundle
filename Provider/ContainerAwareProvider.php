<?php

namespace KG\ScrollerBundle\Provider;

use KG\ScrollerBundle\Scroller\ScrollerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns scrollers from the container based on their aliases.
 *
 * @author Kristen Gilden <gilden@planet.ee>
 */
class ContainerAwareProvider implements ScrollerProviderInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $scroller_ids;

    public function __construct(ContainerInterface $container, array $scroller_ids = array())
    {
        $this->container    = $container;
        $this->scroller_ids = $scroller_ids;
    }

    /**
     * (@inheritDoc)
     */
    public function has($name)
    {
        return isset($this->scroller_ids[$name]);
    }

    /**
     * (@inheritDoc)
     */
    public function get($name)
    {
        if (!$this->has($name)) {
            throw new \InvalidArgumentException(sprintf('The scroller "%s" is not defined.', $name));
        }

        return $this->container->get($this->scroller_ids[$name]);
    }
}