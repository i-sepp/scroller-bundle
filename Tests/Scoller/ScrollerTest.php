<?php

namespace KG\ScrollerBundle\Tests\Scroller;

use KG\ScrollerBundle\Scroller\Scroller;
use KG\ScrollerBundle\Scroller\ScrollerInterface;

/**
 * @author Kristen Gilden <gilden@planet.ee>
 */
class ScrollerTest extends \PHPUnit_Framework_TestCase
{
    protected $scroller;

    protected function setUp()
    {
        $this->scroller = $this->getMockScroller();
    }

    /**
     * @dataProvider validTypeProvider
     */
    public function testSetValidTypes($type, $expected)
    {
        $this->scroller->setType($type);

        $this->assertEquals($expected, $this->scroller->getType());
    }

    /**
     * @dataProvider truePosProvider
     */
    public function testHasPosReturnsTrue($type, $position)
    {
        $this->scroller->setType($type);

        $this->assertTrue($this->scroller->hasAt($position));
    }

    /**
     * @dataProvider falsePosProvider
     */
    public function testHasPosReturnsFalse($type, $position)
    {
        $this->scroller->setType($type);

        $this->assertFalse($this->scroller->hasAt($position));
    }

    public function testLazyLoadingCalledOnce()
    {
        $this->scroller->setType(ScrollerInterface::SIDE_TOP);
        $this->scroller->expects($this->once())->method('load');

        $this->scroller->getAt(ScrollerInterface::POS_TOP_MID);
        $this->scroller->getAt(ScrollerInterface::POS_TOP_MID);
    }

    public function validTypeProvider()
    {
        return array(
            array(ScrollerInterface::SIDE_TOP, 16),
            array(ScrollerInterface::SIDE_LEFT, 32),
            array(ScrollerInterface::SIDE_RIGHT, 64),
            array(ScrollerInterface::SIDE_BOT, 128),
            array(ScrollerInterface::SIDE_TOP | ScrollerInterface::SIDE_LEFT, 49),
            array(ScrollerInterface::SIDE_TOP | ScrollerInterface::SIDE_BOT, 144),
        );
    }

    public function truePosProvider()
    {
        return array(
            array(ScrollerInterface::SIDE_TOP, ScrollerInterface::POS_TOP_MID),
            array(ScrollerInterface::SIDE_TOP | ScrollerInterface::SIDE_LEFT, ScrollerInterface::POS_TOP_LEFT),
            array(ScrollerInterface::SIDE_TOP | ScrollerInterface::SIDE_LEFT, ScrollerInterface::POS_TOP_MID),
            array(ScrollerInterface::SIDE_TOP | ScrollerInterface::SIDE_BOT, ScrollerInterface::POS_BOT_MID),
        );
    }

    public function falsePosProvider()
    {
        return array(
            array(ScrollerInterface::SIDE_TOP, ScrollerInterface::POS_TOP_RIGHT),
            array(ScrollerInterface::SIDE_TOP | ScrollerInterface::SIDE_LEFT, ScrollerInterface::POS_MID_RIGHT),
            array(ScrollerInterface::SIDE_TOP | ScrollerInterface::SIDE_LEFT, ScrollerInterface::POS_BOT_LEFT),
            array(ScrollerInterface::SIDE_TOP | ScrollerInterface::SIDE_BOT, ScrollerInterface::POS_BOT_RIGHT),
        );
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockScroller()
    {
        return $this->getMockForAbstractClass('KG\\ScrollerBundle\\Scroller\\Scroller');
    }
}