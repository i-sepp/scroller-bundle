/**
 * Scroller manager manages all scrolling systems on the page.
 *
 * Copyright 2012, Kristen Gilden <gilden@planet.ee>
 */

(function (context, window, undefined) {

    var Manager = {};

    var cfg = {
        autoboot: true,
        blocks_x:        '.top.mid .movable, .bot.mid .movable',
        blocks_y:        '.cen.left .movable, .cen.right .movable',
        class_draggable: '.draggable',
        class_h_master:  '.draggable', // The element from which the height is taken
        class_w_master:  '.draggable', // The element from which the width is taken
        class_movable:   '.movable',
        class_widget:    '.scroller',
        class_section:   '.section',
        blocks: {
            top_mid:     '.top.mid',
            cen_left:    '.cen.left',
            cen_mid:     '.cen.mid',
            cen_right:   '.cen.right',
            bot_mid:     '.bot.mid',
            center:      '.cen',
            middle:      '.mid',
        },
        touch: {
            delay: 100, // The delay in ms to wait until the event is considered dragging
            last:  -1,  // Last time the user touched down
        }
    };

    /**
     * All scroll boxes
     */
    var $widgets = [];

    /**
     * The boot function.
     */
    Manager.boot = function() {
        var $elements = $(cfg.class_widget);

        $elements.each(function () {
            var $widget = $(this);

            initScroll($widget);
            resetWidgetSize($widget);
            updateContainment($widget.find(cfg.blocks.cen_mid));

            $widgets.push($widget);
        });

        // Listens for screen resizing. `resizeEndHandler` will be
        // called when the resizing stops.
        var timer = undefined;

        $(window).bind('resize', function (e) {
            clearTimeout(timer);
            timer = setTimeout(function () { resizeEndHandler(e); }, 100);
        });
    };

    /**
     * Resets the containments of all the scrollers.
     */
    Manager.resetContainments = function () {
        for (var i in $widgets) {
            resetWidgetSize($widgets[i]);
            updateContainment($widgets[i].find(cfg.blocks.cen_mid));
        }
    };

    /**
     * Resets the positions of all the scrollers.
     */
    Manager.resetPositions = function () {
        for (var i in $widgets) {
            resetWidgetPositions($widgets[i]);
        }
    };

    /**
     * Sets the element's selector from which the height is taken. By default
     * this is the middle component. However, one some occasion it is
     * required to be taken from some other element.
     *
     * @param class_name
     */
    Manager.setHeightMasterClass = function (class_name) {
        cfg.class_h_master = class_name;
    };

    /**
     * Sets the element's selector from which the width is taken.
     *
     * @param class_name
     */
    Manager.setWidthMasterClass = function (class_name) {
        cfg.class_w_master = class_name;
    };

    /**
     * @returns configuration object
     */
    Manager.getCfg = function() {
        return cfg;
    };

    /**
     * Initialize the scrolling for a widget.
     *
     * @param jQuery $widget
     */
    function initScroll($widget) {
        var $container  = $widget.find(cfg.blocks.cen_mid);
        var $draggable  = $container.find(cfg.class_draggable);

        // Find vertical and horizontal objects that must be also dragged
        // when the middle object is dragged.

        var blocks_x = $widget.find(cfg.blocks_x).get();
        var blocks_y = $widget.find(cfg.blocks_y).get();

        // Function to be called when the middle block is dragged
        var move_fun = function () {
            moveX(blocks_x, this.style.left);
            moveY(blocks_y, this.style.top);
        };

        $draggable.draggable({drag: move_fun, stop: move_fun});
    }

    /**
     * Event handler for the window rezise end event. The dimensions of the
     * component as well as the containment sizes must be recalculated.
     *
     * @param e
     */
    function resizeEndHandler(e) {
        var $widget;

        for (var i in $widgets) {
            $widget = $widgets[i];

            resetWidgetPositions($widget);
            resetWidgetSize($widget);
            updateContainment($widget.find(cfg.blocks.cen_mid));
        }
    }

    /**
     * Resizes the scroller component to match its container size.
     *
     * @param $widget    The scroller component
     */
    function resetWidgetSize($widget) {

        var widget_w = $widget.width();
        var widget_h = $widget.height();
        var margin_x = 0;
        var margin_y = 0;

        $widget.find(cfg.blocks.cen_left + ', ' + cfg.blocks.cen_right).each(function () {
            margin_x += $(this).outerWidth();
        });

        $widget.find(cfg.blocks.top_mid + ', ' + cfg.blocks.bot_mid).each(function () {
           margin_y += $(this).outerHeight();
        });

        // The middle components cannot be wider than the width of the
        // center component's draggable part.

        var $master_h  = $widget.find(cfg.class_h_master);
        var $master_w  = $widget.find(cfg.class_w_master);
        var max_width  = $master_w.outerWidth();
        var max_height = $master_h.outerHeight();
        var width      = widget_w - margin_x > max_width ? max_width : widget_w - margin_x;
        var height     = widget_h - margin_y > max_height ? max_height : widget_h - margin_y;

        $widget.find(cfg.blocks.middle).width(width);
        $widget.find(cfg.blocks.center).height(height);
    }

    /**
     * Resets the all the components to their initial position.
     */
    function resetWidgetPositions($widget) {
        var blocks_x = $widget.find(cfg.blocks_x).get();
        var blocks_y = $widget.find(cfg.blocks_y).get();
        var middle   = $widget.find(cfg.class_draggable)[0];

        middle.style.top = middle.style.left = 0;
        moveX(blocks_x, 0);
        moveY(blocks_y, 0);
    }

    /**
     * Returns the coordinates of the containment box. The returned object
     * structure is:
     *
     * {
     *     coords:     [x1, y1, x2, y2],
     *     x_disabled: true,
     *     y_disabled: true
     * },
     *
     * where [x1, y1] are top-left and [x2, y2] are bottom-left coordinates
     * of the containment. `x_disabled` and `y_disabled` determine whether
     * the widget should be allowed to be dragged along that axis.
     *
     * The specified coordinates are given relative to the container's
     * position.
     */
    function getContainment($container, $draggable) {
        var result = {coords: [], x_disabled: false, y_disabled: false};
        var offset_x = $container.offset().left;
        var offset_y = $container.offset().top;
        var pad_x    = parseInt($container.css('padding-left'), 10);
        var pad_y    = parseInt($container.css('padding-top'), 10);

        result.coords[0] = offset_x + $container.width() - $draggable.outerWidth(true) + pad_x;
        result.coords[1] = offset_y + $container.height() - $draggable.outerHeight(true) + pad_y;
        result.coords[2] = offset_x + pad_x;
        result.coords[3] = offset_y + pad_y;

        if ($draggable.outerWidth(true) <= $container.width()) {
            result.x_disabled = true;
        }

        if ($draggable.outerHeight(true) <= $container.height()) {
            result.y_disabled = true;
        }

        return result;
    }

    /**
     * Updates the containment size of a widget. It is assumed that
     * `$draggable` has already been made draggable.
     *
     * @param $container The wrapper around the draggable componenet.
     *
     */
    function updateContainment($container) {
        var $draggable = $container.find(cfg.class_draggable);
        var containment = getContainment($container, $draggable);

        $draggable.draggable('option', 'containment', containment.coords);

        if (containment.x_disabled && containment.y_disabled) {
            $draggable.draggable('option', 'disabled', true);
        } else if (containment.x_disabled) {
            $draggable.draggable('option', 'disabled', false);
            $draggable.draggable('option', 'axis', 'y');
        } else if (containment.y_disabled) {
            $draggable.draggable('option', 'disabled', false);
            $draggable.draggable('option', 'axis', 'x');
        } else {
            $draggable.draggable('option', 'disabled', false);
            $draggable.draggable('option', 'axis', false);
        }
    }

    /**
     * Changes the left style of all the elements in the array to the
     * specified value.
     */
    function moveX(elements, position) {
        for (var i in elements) {
            elements[i].style.left = position;
        }
    }

    /**
     * Changes the top style of all the elements in the array to the
     * specified value.
     */
    function moveY(elements, position) {
        for (var i in elements) {
            elements[i].style.top = position;
        }
    }

    KG.register(Manager, 'KG.Scroller.Manager');
})(KG, window);
