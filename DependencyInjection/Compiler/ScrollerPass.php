<?php

namespace KG\ScrollerBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Scroller compiler pass
 *
 * @author Kristen Gilden <gilden@planet.ee>
 */
class ScrollerPass implements CompilerPassInterface
{
    /**
     * Adds all defined scroller services to the provider.
     *
     * (@inheritDoc)
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('kg_scroller.provider')) {
            return;
        }

        $definition = $container->getDefinition('kg_scroller.provider');

        $scrollers = array();

        foreach ($container->findTaggedServiceIds('kg_scroller.scroller') as $id => $tags) {
            foreach ($tags as $attributes) {
                if (empty($attributes['alias'])) {
                    throw new \InvalidArgumentException(sprintf('The alias is not defined in the "kg_scroller.scroller" tag for the service "%s"', $id));
                }

                $scrollers[$attributes['alias']] = $id;
            }
        }

        $definition->replaceArgument(1, $scrollers);
    }
}