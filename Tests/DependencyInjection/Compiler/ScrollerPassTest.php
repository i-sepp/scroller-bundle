<?php

namespace KG\ScrollerBundle\Tests\DependencyInjection\Compiler;

use KG\ScrollerBundle\DependencyInjection\Compiler\ScrollerPass;

/**
 * @author Kristen Gilden <gilden@planet.ee>
 */
class ScrollerPassTest extends \PHPUnit_Framework_TestCase
{
    public function testProcessWithoutProviderDefinition()
    {
        $scroller_pass = new ScrollerPass();

        $this->assertNull($scroller_pass->process($this->getMockContainerBuilder()));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testProcessWithEmptyAlias()
    {
        $container_builder = $this->getMockContainerBuilder();
        $container_builder->expects($this->once())
            ->method('hasDefinition')
            ->will($this->returnValue(true));
        $container_builder->expects($this->once())
            ->method('findTaggedServiceIds')
            ->with($this->equalTo('kg_scroller.scroller'))
            ->will($this->returnValue(array('id' => array('tag1' => array('alias' => '')))));

        $scroller_pass = new ScrollerPass();
        $scroller_pass->process($container_builder);
    }

    public function testProcessWithAlias()
    {
        $definition = $this->getMockDefinition();
        $definition->expects($this->once())
            ->method('replaceArgument')
            ->with($this->equalTo(1), $this->equalTo(array('test_alias' => 'id')));

        $container_builder = $this->getMockContainerBuilder();
        $container_builder->expects($this->once())
            ->method('hasDefinition')
            ->will($this->returnValue(true));
        $container_builder->expects($this->once())
            ->method('findTaggedServiceIds')
            ->with($this->equalTo('kg_scroller.scroller'))
            ->will($this->returnValue(array('id' => array('tag1' => array('alias' => 'test_alias')))));
        $container_builder->expects($this->once())
            ->method('getDefinition')
            ->with($this->equalTo('kg_scroller.provider'))
            ->will($this->returnValue($definition));

        $scroller_pass = new ScrollerPass();
        $scroller_pass->process($container_builder);
    }

    public function getMockDefinition()
    {
        $mock = $this->getMockBuilder('Symfony\\Component\\DependencyInjection\\Definition')
            ->disableOriginalConstructor()
            ->getMock();

        return $mock;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockContainerBuilder()
    {
        return $this->getMock('Symfony\Component\DependencyInjection\ContainerBuilder');
    }
}