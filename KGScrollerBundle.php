<?php

namespace KG\ScrollerBundle;

use KG\ScrollerBundle\DependencyInjection\Compiler\ScrollerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class KGScrollerBundle extends Bundle
{
    /**
     * (@inheritDoc)
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ScrollerPass());
    }
}
