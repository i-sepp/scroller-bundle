/**
 * KG Javascript Library
 *
 * The core handles all other components.
 *
 * Copyright 2012, Kristen Gilden <gilden@planet.ee>
 */

(function( context, undefined ) {

    // If the KG namespace is already existing, do not
    // construct it again.
    if (undefined !== context.KG) {
        return;
    }

    var boot_stack = [];
    var KG = {
        Core : {}
    };

    /**
     * Registers a new comonent to the KG namespace.
     *
     * @param string component
     * @param string name The full namespace, e.g 'KG.Scroller.Vertical'
     */
    KG.register = function (component, name) {
        var parent;
        var current;
        name = name.split('.');

        if ('KG' !== name.shift()) {
            throw 'All namespaces must begin with `KG.`';
        }

        parent = KG;

        // Add any new namespace objects
        while (undefined !== name[1]) {
            current = name.shift();

            if (undefined === parent[current]) {
                parent[current] = {};
            }

            parent = parent[current];
        }

        // Last name should denote the component itself
        name = name[0];

        if (undefined !== parent[name]) {
            throw 'cannot overwrite component ' + name;
        }

        if (undefined === component.getCfg) {
            throw name + ' is missing function `getCfg()`';
        }

        parent[name] = component;

        if(component.getCfg().autoboot === true) {
            if (undefined === component.boot) {
                throw name + ' is missing function `boot()`';
            }

            boot_stack.push(component.boot);
        }
    };

    /**
     * Initializes all the necessary components.
     */
    KG.Core.boot = function() {
        for (var i in boot_stack) {
            boot_stack[i]();
        }
    };

    // Expose KG to the global object
    context.KG = KG;
})(window);