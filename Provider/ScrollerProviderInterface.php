<?php

namespace KG\ScrollerBundle\Provider;

use KG\ScrollerBundle\Scroller\ScrollerInterface;

/**
 * Provides instances of ScrollerInterface. Each instance
 * is a separate scrolling system.
 *
 * @author Kristen Gilden <gilden@planet.ee>
 */
interface ScrollerProviderInterface
{
    /**
     * Returns whether the scroller is registered in the provider.
     *
     * @param string $name
     *
     * @return boolean
     */
    function has($name);

    /**
     * Gets a scroller from the provider. Throws a ScrollerNotFoundException
     * if no scroller was not found.
     *
     * @param string $name
     * @throws ScrollerNotFoundException
     */
    function get($name);
}