<?php

namespace KG\ScrollerBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

use KG\ScrollerBundle\Provider\ScrollerProviderInterface;
use KG\ScrollerBundle\Scroller\ScrollerInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Helper function for use in Twig templates.
 *
 * @author Kristen Gilden <gilden@planet.ee>
 */
class ScrollerExtension extends \Twig_Extension
{
    /**
     * @var array
     */
    protected $positions;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * (@inheritDoc)
     */
    public function getName()
    {
        return 'ScrollerExtension';
    }

    /**
     * (@inheritDoc)
     */
    public function getTests()
    {
        return array(
            'setAt' => new \Twig_Test_Method($this, 'isSetAt'),
        );
    }

    /**
     * (@inheritDoc)
     */
    public function getFunctions()
    {
        return array(
            'kg_scroller_render'     => new \Twig_Function_Method($this, 'renderScroller', array('is_safe' => array('all'))),
            'kg_scroller_render_pos' => new \Twig_Function_Method($this, 'renderPosition', array('is_safe' => array('all'))),
        );
    }

    /**
     * Returns whether the scroller set at the specified position.
     *
     * @param ScrolleInterface $scroller
     * @param string $position
     *
     * @return boolean
     */
    public function isSetAt(ScrollerInterface $scroller, $position)
    {
        if (!isset($this->positions)) {
            $this->init();
        }

        if (!isset($this->positions[$position])) {
            $message = '%s is an invalid position, only %s are allowed';
            $message = sprintf($message, $position, implode(', ', array_keys($this->positions)));

            throw new \InvalidArgumentException($message);
        }

        return $scroller->hasAt($this->positions[$position]);
    }

    /**
     * Renders a scroller.
     *
     * @param string $name    Alias of the scroller service
     * @param array $params Any additional parameters required for the scroller
     *
     * @return string
     */
    public function renderScroller($name, array $params = array())
    {
        $provider = $this->container->get('kg_scroller.provider');
        $renderer = $this->container->get('templating');

        $scroller = $provider->get($name);
        $scroller->setParameters($params);

        $parameters = array(
            'scroller' => $scroller,
        );

        return $renderer->render('KGScrollerBundle:Scroller:layout.html.twig', $parameters);
    }

    /**
     * Renders the specified position of the scroller.
     *
     * @param ScrollerInterface $scroller
     * @param string $position
     *
     * @return string
     */
    public function renderPosition(ScrollerInterface $scroller, $position)
    {
        if (!isset($this->positions)) {
            $this->init();
        }

        return $scroller->getAt($this->positions[$position]);
    }

    /**
     * Creates a map for positions
     */
    protected function init()
    {
        $this->positions = array(
            'top_left'  => ScrollerInterface::POS_TOP_LEFT,
            'top_mid'   => ScrollerInterface::POS_TOP_MID,
            'top_right' => ScrollerInterface::POS_TOP_RIGHT,
            'mid_left'  => ScrollerInterface::POS_MID_LEFT,
            'mid_mid'   => ScrollerInterface::POS_MID_MID,
            'mid_right' => ScrollerInterface::POS_MID_RIGHT,
            'bot_left'  => ScrollerInterface::POS_BOT_LEFT,
            'bot_mid'   => ScrollerInterface::POS_BOT_MID,
            'bot_right' => ScrollerInterface::POS_BOT_RIGHT,
        );
    }
}